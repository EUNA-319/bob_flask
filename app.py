from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello Bob from user028'

@app.route('/add')
def add():
    a = request.args.get('a', type=int)
    b = request.args.get('b',type=int)
    return str(a+b)

@app.route('/sub')
def sub():
    a = request.args.get('a',type=int)
    b = request.args.get('b',type=int)
    return str(a-b)

if __name__ =="__main__":
    app.run(host='0.0.0.0', port=8028)